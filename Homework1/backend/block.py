from hashlib import sha256
import json


class Block:
    def compute_hash(self):
        block_string = json.dumps({'index': self.index,
                                   'transactions': self.transactions,
                                   'previous_hash': self.previous_hash,
                                   'nonce': self.nonce,
                                   'timestamp': self.timestamp
                                   }, sort_keys=True)
        return sha256(block_string.encode()).hexdigest()

    def __init__(self, index, transactions, timestamp, previous_hash):
        self.index = index
        self.transactions = transactions
        self.timestamp = timestamp
        self.previous_hash = previous_hash
        self.hash = ""
        self.nonce = 0

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"index {self.index}\n " \
               f"transactions {self.transactions}\n," \
               f"timestamp {self.timestamp}," \
               f"previous_hash {self.previous_hash}" \
               f"hash {self.hash}" \
               f"nonce {self.nonce}"




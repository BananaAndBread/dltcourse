import time

import requests

from block import Block


def create_first_block():
    # block = Block(index=0, transactions=[], timestamp=time.time(), previous_hash="")
    block = Block(index=0, transactions=[], timestamp=0, previous_hash="")
    block.hash = block.compute_hash()
    return block


def dict_to_block(d):
    block = Block(
        index=d['index'],
        transactions=d['transactions'],
        timestamp=d['timestamp'],
        previous_hash=d['previous_hash']
    )
    if 'hash' in d:
        block.hash = d['hash']
    if 'nonce' in d:
        block.nonce = d['nonce']
    return block

class Blockchain:
    num_of_zeros_in_start = 2
    transactions_num_in_block = 10
    def __init__(self, ):
        self.chain = []
        first_block = create_first_block()
        self.chain.append(first_block)
        self.transaction_queue = []


    def get_last_block(self):
        return self.chain[-1]

    @staticmethod
    def find_hash(block):
        # handle someone already found hash?
        block.nonce = 0
        computed_hash = block.compute_hash()
        while not computed_hash.startswith('0' * Blockchain.num_of_zeros_in_start):
            block.nonce += 1
            computed_hash = block.compute_hash()
        return computed_hash

    @staticmethod
    def is_block_hash_valid(block):
        return (block.hash.startswith('0' * Blockchain.num_of_zeros_in_start) and
                block.hash == block.compute_hash())

    def is_appended_to_last_block(self, block):
        last_block_hash = self.get_last_block().hash
        return block.previous_hash == last_block_hash

    def is_new_block_valid(self, block):
        return self.is_appended_to_last_block(block) and self.is_block_hash_valid(block)

    def add_block(self, block):
        print(block)
        print(self.chain)
        if not self.is_new_block_valid(block):
            return False
        self.chain.append(block)
        return True

    def add_new_transaction(self, transaction):
        self.transaction_queue.append(transaction)

    def mine(self):
        if len(self.transaction_queue) == 0:
            return False
        last_block = self.get_last_block()
        new_block = Block(index=last_block.index+1,
                          transactions=self.transaction_queue[0:Blockchain.transactions_num_in_block],
                          timestamp=time.time(),
                          previous_hash=last_block.hash)
        new_block.hash = self.find_hash(new_block)
        print(self.add_block(new_block))
        self.transaction_queue = self.transaction_queue[Blockchain.transactions_num_in_block:]
        return True

    @staticmethod
    def chain_is_connected(chain):
        previous_hash = ""
        for block in chain:
            if block.previous_hash != previous_hash:
                return False
            previous_hash = block.hash
        return True

    @staticmethod
    def block_hashes_valid(chain):
        for block in chain:
            if not Blockchain.is_block_hash_valid(block):
                return False
        return True

    @staticmethod
    def check_chain_validity(chain):
        if not (Blockchain.chain_is_connected(chain) and Blockchain.block_hashes_valid(chain)):
            return False
        return True

    @staticmethod
    def consensus(peers, blockchain):
        """
        Our simple consensus algorithm. If a longer valid chain is
        found, our chain is replaced with it.
        """
        longest_chain = None
        current_len = 0

        for node in peers:
            response = requests.get('{}/chain'.format(node))
            length = response.json()['length']
            chain = list(map(dict_to_block, response.json()['chain']))
            if length > current_len and Blockchain.check_chain_validity(chain):
                current_len = length
                longest_chain = chain

        if longest_chain:
            blockchain.chain = longest_chain
            return blockchain

        return blockchain





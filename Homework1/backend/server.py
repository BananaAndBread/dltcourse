import json
import time

from flask import Flask, request
import requests
from blockchain import Blockchain
from block import Block
from flask_cors import CORS

app =  Flask(__name__)
CORS(app)
blockchain = Blockchain()
peers = set()

@app.route('/new_transaction', methods=['POST'])
def new_transaction():
    received_transaction = request.get_json()
    required_fields = ["author", "content"]

    for field in required_fields:
        if not received_transaction.get(field):
            return f"Field {field} is required", 404

    received_transaction["timestamp"] = time.time()
    blockchain.add_new_transaction(received_transaction)

    return "Transaction received", 200


@app.route('/get_chain', methods=['GET'])
def get_chain():
    chain_data = []
    for block in blockchain.chain:
        chain_data.append(block.__dict__)
    return json.dumps({"length": len(chain_data),
                       "chain": chain_data})


@app.route('/mine', methods=['GET'])
def mine_transaction_queue():
    global blockchain
    result = blockchain.mine()
    if not result:
        return "No transactions to mine"
    else:
        chain_length = len(blockchain.chain)
        blockchain = Blockchain.consensus(peers, blockchain)
        if chain_length == len(blockchain.chain):
            announce_new_block(blockchain.get_last_block())
        return "Block #{} is mined.".format(blockchain.get_last_block().index)


@app.route('/register_node', methods=['POST'])
def register_new_peers():
    node_address = request.get_json()["node_address"]
    if not node_address:
        return "Invalid data", 400

    peers.add(node_address)
    return serialize_peers(peers)


def serialize_peers(peers):
    return json.dumps({"peers": list(peers)})

@app.route('/add_block', methods=['POST'])
def verify_and_add_block():
    block_data = request.get_json()
    block = Block(block_data["index"],
                  block_data["transactions"],
                  block_data["timestamp"],
                  block_data["previous_hash"])

    block.hash = block_data['hash']
    block.nonce = block_data['nonce']
    added = blockchain.add_block(block)

    if not added:
        return "Block is incorrect", 400

    return "Block added to the chain", 200


def announce_new_block(block):
    for peer in peers:
        url = "{}/add_block".format(peer)
        requests.post(url, data=json.dumps(block.__dict__, sort_keys=True), headers={'content-type': 'application/json'})


